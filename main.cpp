#include <iostream>
#include <string>
#include <string.h>
using namespace std;

class Person
{
private:
    string firstName;
    string lastName;
    int age;
    int *weight;
    int count;
public:
    Person(string first, string last,int age)
    {
       if(age<=0 || age>120)
           throw 1;
       firstName=first;
       lastName=last;
       this->age=age;
       weight=0; // nullptr, NULL
       count=0;
    }
    //Person(string first, string last,int age):
    //   firstName(first),
    //   lastName(last),
    //   this->age(age),
    //   weight(0),
    //   count(0) {}

    void addWeight(int value) {
        if(count==0)
            weight=new int[++count];
        else {
            int *temp=new int[++count];
            for(int i=0;i<count-1;i++)
                temp[i]=weight[i];
            delete[] weight;
            weight=temp;
        }
        weight[count-1]=value;
    }
    float getAvWeight() const {
        float sum=0;
        for(int i=0;i<count;i++)
            sum+=weight[i];
        if(count==0)
            throw 0;
        return sum/count;
    }
    ~Person() {
        delete[] weight;
    }
};

int main()
{
    try
    {
      Person vasya("Vasya","Pupkin",12);
      cout<<vasya.getAvWeight()<<endl;
    
      vasya.addWeight(70);
      vasya.addWeight(73);
      vasya.addWeight(71);
    }
    catch(int error) {
        if(error==0)
            cout<<"Array is empty!"<<endl;
        else if(error==1)
            cout<<"Age is not correct!"<<endl;
        else
            cout<<"Unknown error!"<<endl;
    }
    return 0;
}
